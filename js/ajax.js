$(document).ready(function () {
    for (let i = 1; i <= 9; i++) {
        $.ajax({
            type: "GET",
            url: `https://swapi.co/api/people/?page=${i}`,
            contentType: "json",
            success: function (data) {
                $.each(data.results, function (index, elem) {
                    let string = `<div class="film-item" id="${elem.name.replace(/ /g, '')}">
                <p class="item">Имя: ${elem.name}</p><p class="item">Пол: ${elem.gender}</p>`;
                    $.ajax({
                        type: "GET",
                        url: elem.homeworld,
                        contentType: "json",
                        success: function (dataWorld) {
                            string += `<p class="item">Родной мир: ${dataWorld.name}</p>`;
                            if (elem.starships.length >= 1) {
                                string += `<button class="button" data-ship="${elem.starships}" id="button">Список кораблей
                            </button></div>`;
                            } else {
                                string += `</div>`;
                            }
                            $("#film-cont").append(`${string}`);
                        },
                        error: function (xhr) {
                            alert(xhr.status);
                        }
                    });
                });
            },
            error: function (xhr) {
                alert(xhr.status);
            }
        });
    }

    $(document).on("click", function (e) {
        let eventTarget;
        let eventId;
        let dataShip;
        if ($(e.target).hasClass("button")) {
            eventTarget = e.target;
            eventId = $(eventTarget).parent().attr('id');
            dataShip = $(eventTarget).attr("data-ship").split(",");
        }
        $.each(dataShip, function (i, elem) {
            $.ajax({
                type: "GET",
                url: elem,
                contentType: "json",
                success: function (dataShip) {
                    let falcon;
                    if (dataShip.name === "Millennium Falcon") {
                        falcon = "<h2>Хан Соло стрелял первым!</h2>"
                    } else falcon = "";
                    $(`#${eventId}`).append(`<a href="#" class="item-link" data-ship="${dataShip.model}, ${dataShip.starship_class}, 
                    ${dataShip.passengers}, ${dataShip.manufacturer}">${dataShip.name}</a>${falcon}`);
                },
                error: function (xhr) {
                    alert(xhr.status);
                }
            });
        });
        $(eventTarget).replaceWith("<h3>Пилотируемые корабли:</h3>");

        if ($(e.target).hasClass("item-link")) {
            e.preventDefault();
            eventTarget = e.target;
            dataShip = $(eventTarget).attr("data-ship").split(", ");
            $(eventTarget).append(`<p>Модель: ${dataShip[0]}</p><p>Класс: ${dataShip[1]}</p>
                    <p>Пассажиры: ${dataShip[2]}</p><p>Производитель: ${dataShip[3]}</p>`);
            $(eventTarget).css("pointer-events", 'none');
        }
    });
});